from django.db import models
from django.urls import reverse

class AutomobileVO(models.Model):
    vin = models.CharField(max_length=17, unique=True)
    sold = models.BooleanField(default=False)

    def __str__(self):
        return f"{self.vin}, {self.sold}, {self.id}"

class Salesperson(models.Model):
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
    employee_id = models.CharField(max_length=100, unique=True)

    def __str__(self):
        return f"{self.last_name}, {self.first_name}, {self.id}"

    def get_api_url(self):
        return reverse("api_saleperson", kwargs={"employee_id": self.employee_id})

class Customer(models.Model):
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
    address = models.CharField(max_length=200)
    phone_number = models.CharField(max_length=16)

    def __str__(self):
        return f"{self.last_name}, {self.first_name}"

    def get_api_url(self):
        return reverse("api_customer", kwargs={"pk": self.id})

class Sale(models.Model):
    automobile = models.ForeignKey(
        AutomobileVO,
        related_name="sales",
        on_delete=models.PROTECT,
        default=None
    )
    salesperson = models.ForeignKey(
        Salesperson,
        related_name="sales",
        on_delete=models.PROTECT,
        default=None
    )
    customer = models.ForeignKey(
        Customer,
        related_name="sales",
        on_delete=models.PROTECT,
         default=None
    )
    price = models.FloatField()

    def __str__(self):
        return f"{self.automobile}, {self.price}"

    def get_api_url(self):
        return reverse("api_sale", kwargs={"pk": self.id})
