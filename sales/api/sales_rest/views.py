from django.views.decorators.http import require_http_methods
import json
from django.http import JsonResponse

from .models import  Salesperson, Customer, Sale, AutomobileVO

from .encoders import (SalespersonEncoder, CustomerEncoder, SaleEncoder)
import requests

@require_http_methods(["GET", "POST"])
def api_salespeople(request):
    if request.method == "GET":
        try:
            salespeople = Salesperson.objects.all()
            return JsonResponse(
                {"salespeople": salespeople},
                encoder=SalespersonEncoder,
            )
        except Salesperson.DoesNotExist:
            response = JsonResponse(
                {"message": "Salesperson does not exist"},
                status = 404)
            return response
    else: #POST
        try:
            content = json.loads(request.body)
            employee_id = content["employee_id"]
            if Salesperson.objects.filter(employee_id=employee_id).exists():
                raise Salesperson.DoesNotExist
            else:
                salesperson = Salesperson.objects.create(**content)
                return JsonResponse(
                {"salesperson": salesperson},
                encoder=SalespersonEncoder,
            )
        except Salesperson.DoesNotExist:
            response = JsonResponse(
                {"message":"Employee ID already exist. Try again."},
                status=400
                )
            return response
        except Exception as e:
                response = JsonResponse(
                {"message": "Could not create the salesperson: " + str(e)},
                status=400
            )
                return response

@require_http_methods(["DELETE", "GET"])
def api_salesperson(request, employee_id):
    if request.method == "GET":
        try:
            salesperson = Salesperson.objects.get(employee_id=employee_id)
            return JsonResponse(
                salesperson,
                encoder=SalespersonEncoder,
                safe=False
            )
        except Salesperson.DoesNotExist:
            response = JsonResponse(
                {"message": "Salesperson does not exist"},
                status = 404
                )
            return response
    else: #Delete
        try:
            employee = Salesperson.objects.get(employee_id=employee_id)
            employee.delete()
            return JsonResponse(
                employee,
                encoder=SalespersonEncoder,
                safe=False,
            )
        except Salesperson.DoesNotExist:
            response = JsonResponse(
                {"message": "Salesperson does not exist"},
                status = 404
            )
            return response

        except Exception as e:
            response = JsonResponse(
                {"message": "Could not delete the salesperson: " + str(e)},
                status=400
            )
            return response

@require_http_methods(["GET", "POST"])
def api_customers(request):
    if request.method == "GET":
        try:
            customers = Customer.objects.all()
            return JsonResponse(
                {"customers": customers},
                encoder=CustomerEncoder,
            )
        except Customer.DoesNotExist:
            response = JsonResponse(
                {"message": "Customer does not exist"},
                status = 4040
                )
            return response
    else: #POST
        try:
            content = json.loads(request.body)
            customers = Customer.objects.create(**content)
            return JsonResponse(
                customers,
                encoder=CustomerEncoder,
                safe=False,
            )
        except Exception as e:
            response = JsonResponse(
                {"message": "Could not create the customer: " + str(e)},
                status= 400
            )
            return response

@require_http_methods(["DELETE", "GET"])
def api_customer(request, pk):
    if request.method == "GET":
        try:
           customer = Customer.objects.get(id=pk)
           return JsonResponse(
                customer,
                encoder=CustomerEncoder,
                safe=False,
            )
        except Customer.DoesNotExist:
            response = JsonResponse(
                {"message": "Customer does not exist"},
                status = 404
            )
            return response
    else: #delete
        try:
            customer = Customer.objects.get(id=pk)
            customer.delete()
            response = JsonResponse(
                customer,
                encoder=CustomerEncoder,
                safe=False,
            )
            return response
        except Customer.DoesNotExist:
            response = JsonResponse(
                {"message": "Customer does not exist"},
                status = 404
                )
            return response
        except Exception as e:
            response = JsonResponse(
                {"message": "Could not delete the customer: " + str(e)},
                status=400
            )
            return response

@require_http_methods(["GET", "POST"])
def api_sales(request, auto_vo_id=None):
    if request.method == "POST":
        content = json.loads(request.body)
        try:
            automobile_vin = content["automobile_vin"]
            salesperson_id = content["salesperson_id"]
            customer_id = content["customer_id"]
            salesperson = Salesperson.objects.get(employee_id=salesperson_id)
            customer = Customer.objects.get(pk=customer_id)

             # Check if an unsold AutomobileVO with the given VIN exists
            automobile = AutomobileVO.objects.filter(vin=automobile_vin, sold=False).first()

            if not automobile:
                raise AutomobileVO.DoesNotExist

            sale = Sale.objects.create(
                automobile=automobile,
                salesperson=salesperson,
                customer=customer,
                price=content["price"]
            )

            automobile.sold = True
            automobile.save()

            return JsonResponse(
                sale,
                encoder=SaleEncoder,
                safe=False
    )
        except Salesperson.DoesNotExist:
            response = JsonResponse(
                {"message": "Salesperson does not exist."},
                status=404
            )
            return response
        except Customer.DoesNotExist:
            response = JsonResponse(
                {"message": "Customer does not exist."},
                status=404
            )
            return response
        except AutomobileVO.DoesNotExist:
            response = JsonResponse(
                {"message": "Automobile does not exist or is already sold."},
                status=404
            )
            return response
        except Exception as e:
            response = JsonResponse(
                {"message": "Could not create the sale: " + str(e)},
                status=400
            )
            return response
    else: #GET
        try:
            if auto_vo_id is not None:
                sales = Sale.objects.filter(automobile_id=auto_vo_id)
            else:
                sales = Sale.objects.all()
            return JsonResponse(
                {"sales": sales},
                encoder=SaleEncoder,
                safe=False,
            )
        except Sale.DoesNotExist:
                response = JsonResponse(
                {"message": "Sale does not exist."},
                status=404
            )
                return response


@require_http_methods(["DELETE", "GET"])
def api_sale(request, pk):
    if request.method == "GET":
        try:
           sale = Sale.objects.get(id=pk)
           return JsonResponse(
                sale,
                encoder=SaleEncoder,
                safe=False
            )
        except Sale.DoesNotExist:
            response = JsonResponse(
                {"message": "Sale does not exist"},
                status = 404
                )
            return response
    else: #delete
        try:
            sale = Sale.objects.get(id=pk)
            automobile_id = sale.automobile.id
            AutomobileVO.objects.filter(id=automobile_id).update(sold=False)
            sale.delete()

            return JsonResponse(
                {"message": "Sale deleted successfully"},
                status=200
            )
        except Sale.DoesNotExist:
            response= JsonResponse(
                {"message": "Sale does not exist"},
                status = 404
                )
            return response
        except Exception as e:
            response = JsonResponse(
                {"message": "Could not delete the sale: " + str(e)},
                status=400
            )
            return response
