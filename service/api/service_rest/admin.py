from django.contrib import admin
from .models import AutomobileVO, Technician, Appointment


@admin.register(AutomobileVO)
class AutomobileVOAdmin(admin.ModelAdmin):
    list_display = ("id", 'vin', 'sold')


@admin.register(Technician)
class TechnicianAdmin(admin.ModelAdmin):
    list_display = ('id', "first_name", "employee_id")


@admin.register(Appointment)
class AppointmentAdmin(admin.ModelAdmin):
    list_display = ("id", "customer", "reason", "status")
