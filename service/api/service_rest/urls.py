from django.urls import path
from service_rest.views import (api_list_technicians,
                                api_fire_technician,
                                api_list_appoinments,
                                api_delete_appointment,
                                api_cancel_appointment,
                                api_finish_appointment
                                )


urlpatterns = [
    path('technicians/', api_list_technicians, name="api_list_technicians"),
    path("technicians/<str:employee_id>/", api_fire_technician, name="api_fire_technician"),
    path("appointments/", api_list_appoinments, name="api_list_appointments"),
    path("appointments/<id>/", api_delete_appointment, name="api_delete_appointment"),
    path("appointments/<id>/cancel/", api_cancel_appointment, name="api_cancel_appointment"),
    path("appointments/<id>/finish/", api_finish_appointment, name="api_finish_appointment")
]
