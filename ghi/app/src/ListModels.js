
function ModelsList(props) {
    async function deleteModel(id) {
        const url = `http://localhost:8100/api/models/${id}/`;

        const response = await fetch(url, {
          method: 'DELETE',
        });

        if (response.ok) {
          props.setModelsList(newList => newList.filter(model => model.id !== id));
          props.getModelsList()
          props.getAutosList()
        } else {
          console.error("Unable to delete;", response.status);
        }
      }
    return(
      <div className="container m-3">
        <div className="row">
          <div className="col-12 col-md-6">
            <h1>Models</h1>
          </div>
          <div className="col">

          </div>
          <div className="table-responsive mt-3 ">
              <table className="table table-hover">
                    <thead className="bg-success text-white">
                        <tr>
                            <th>Name</th>
                            <th>Manufacturer</th>
                            <th>Picture</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                    {props.modelsList.map((model) => {
                    return(
                    <tr key={model.id}>
                        <td>{model.name}</td>
                        <td>{model.manufacturer.name}</td>
                        <td><img alt={model.name} src={model.picture_url} /></td>
                        <td>
                        <button onClick={() => deleteModel(model.id)} type="button" className="btn btn-outline-secondary">Delete </button>
                        </td>
                        </tr>
                          )})}
                        </tbody>
                    </table>
            </div>
          </div>
        </div>
    )
}

export default ModelsList;
