
function ManufacturersList(props) {
    async function deleteManufacturer(id) {
        const url = `http://localhost:8100/api/manufacturers/${id}/`;

        const response = await fetch(url, {
          method: 'DELETE',
        });

        if (response.ok) {
           props.setManufacturersList(newList => newList.filter(manufacturer => manufacturer.id !== id));
           props.getManufacturersList()
           props.getModelsList()
           props.getAutosList()
        } else {
          console.error("Unable to delete:", response.status);
        }
      }
    return(
        <div className="container m-3">
            <div className="row">
            <div className="col-12 col-md-6">
            <h1>Manufacturers</h1>
            </div>
            <div className="table-responsive mt-3">
            <table className="table table-hover">
                <thead className="bg-success text-white">
                    <tr>
                        <th>Name</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                {props.manufacturersList.map((manufacturer) => {
                return(
                <tr key={manufacturer.id}>
                    <td>{manufacturer.name}</td>
                    <td>
                        <button onClick={() => deleteManufacturer(manufacturer.id)} type="button" className="btn btn-outline-secondary ">Delete</button>
                    </td>
                </tr>
                  )})}
                </tbody>
            </table>
            </div>
        </div>
        </div>
    )
}

export default ManufacturersList;
