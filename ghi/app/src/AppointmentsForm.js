import { useState } from "react";
function AppointmentsForm(props) {
    // const [customer, setCustomer] = useState("");
    // const [vin, setVIN] = useState("");
    // const [reason, setReason] = useState("")
    // const [technician, setTechnician] = useState("");
    // const [time, setTime] = useState("");
        // const handleCustomerChange = event => {
    //     const value = event.target.value;
    //     setCustomer(value);
    // };
    // const handleVINChange = event => {
    //     const value = event.target.value;
    //     setVIN(value);
    // };
    // const handleReasonChange = event => {
    //     const value = event.target.value;
    //     setReason(value);
    // }
    // const handleTechnicianChange = event => {
    //     const value = event.target.value;
    //     setTechnician(value);
    // }
    // const handleTimeChange = event => {
    //     const value = event.target.value;
    //     setTime(value);
    // }
    // async function handleSubmit (event) {
    //     event.preventDefault();
    //     const data = {};
    //     data.customer = customer;
    //     data.vin = VIN;
    //     data.reason = reason;
    //     data.technician = technician;
    //     data.date_time = time;
    // }
    const [form, setForm] = useState({
        customer: "",
        vin: "",
        reason: "",
        technician: "",
        date_time: "",
    });

    const handleChange = (event) => {
        setForm({
            ...form,
            [event.target.name]: event.target.value,
        });
    }

    const handleSubmit = async (event) => {
        event.preventDefault();
        // i really hope this works
        const data = { ...form };

        const url = "http://localhost:8080/api/appointments/"
        const fetchConfig = {
            method: "POST",
            body: JSON.stringify(data),
            headers: {
                "Content-Type": "application/json"
            },
        };
        const response = await fetch(url, fetchConfig);
        if (response.ok) {
            await response.json();
            // console.log("I think technician wont reset because its all in a form")
            setForm({
                customer: "",
                vin: "",
                reason: "",
                technician: "",
                date_time: "",
            });
            // this is how i fix dropdown not reseting
            document.getElementById('technician').selectedIndex = 0;
            // hope this works
            props.getAppointmentList()
        } else {
            console.error("Error:", response)
        }
    };


    return (
            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                    <h1>Add a new appointment</h1>
                        <form onSubmit={handleSubmit} id="create-auto" className="create-auto">
                            <div className="form-floating mb-3">
                                <input onChange={handleChange}
                                value={form.customer}
                                placeholder="Customer"
                                required type="text"
                                name="customer"
                                id="customer"
                                className="form-control"/>
                                <label htmlFor="customer">Customer Name</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={handleChange}
                                value={form.vin}
                                placeholder="vin"
                                required type="text"
                                name="vin"
                                id="vin"
                                className="form-control"/>
                                <label htmlFor="vin">Vehicle VIN</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={handleChange}
                                value={form.reason}
                                placeholder="Reason"
                                required type="text"
                                name="reason"
                                id="reason"
                                className="form-control"/>
                                <label htmlFor="reason">Reason</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={handleChange}
                                value={form.date_time}
                                placeholder="When"
                                required type="datetime-local"
                                name="date_time"
                                id="date_time"
                                className="form-control"/>
                                <label htmlFor="date_time">Time</label>
                            </div>
                            <div className="form-floating mb-3">
                                <select onChange={handleChange} name="technician" id="technician" className="form-select">
                                <option value="">Choose a technician</option>
                                {
                                props.techniciansList.map(technician => {
                                    return (
                                        <option
                                        key={technician.employee_id}
                                        value={technician.employee_id}>
                                            {technician.first_name} {technician.last_name}
                                        </option>
                                    )
                                })}
                                </select>
                                <label htmlFor="technician">Technician</label>
                             </div>
                            <button className="btn btn-primary">Create</button>
                        </form>
                    </div>
                </div>
            </div>
    )
}
export default AppointmentsForm;
