import { NavLink } from 'react-router-dom';

function Nav() {
  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-success">
      <div className="container-fluid">
        <NavLink className="navbar-brand" to="/">CarCar</NavLink>
        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">
            <li className="nav-item">
                <NavLink className="nav-link active" aria-current="page" to="/">Home</NavLink>
            </li>
            <li className="nav-item dropdown">
              <NavLink className="nav-link dropdown-toggle" to="/" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                Manufacturers
              </NavLink>
              <ul className="dropdown-menu">
                <li><NavLink className="dropdown-item" to="/manufacturers/list/">Manufacturer List</NavLink></li>
                <li><NavLink className="dropdown-item" to="/manufacturers/new/">Add Manufacturer</NavLink></li>
              </ul>
            </li>
            <li className="nav-item dropdown">
              <NavLink className="nav-link dropdown-toggle" to="/" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                Vehicle Models
              </NavLink>
              <ul className="dropdown-menu">
                <li><NavLink className="dropdown-item" to="/models/list/">Models List</NavLink></li>
                <li><NavLink className="dropdown-item" to="/models/new/">Add Model</NavLink></li>
              </ul>
            </li>
            <li className="nav-item dropdown">
              <NavLink className="nav-link dropdown-toggle" to="/" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                Automobiles
              </NavLink>
              <ul className="dropdown-menu">
                <li><NavLink className="dropdown-item" to="/autos/list/"> Automobiles List</NavLink></li>
                <li><NavLink className="dropdown-item" to="/autos/new/">Add  Automobile</NavLink></li>
              </ul>
            </li>
            <li className="nav-item dropdown">
              <NavLink className="nav-link dropdown-toggle" to="/" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                Salespeople
              </NavLink>
              <ul className="dropdown-menu">
                <li><NavLink className="dropdown-item" to="/salespeople/list/">Salesperson List</NavLink></li>
                <li><NavLink className="dropdown-item" to="/salespeople/new/">Add Salesperson</NavLink></li>
                <li><NavLink className="dropdown-item" to="/salespeople/history/">Salesperson History</NavLink></li>
              </ul>
            </li>
            <li className="nav-item dropdown">
              <NavLink className="nav-link dropdown-toggle" to="/" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                Customers
              </NavLink>
              <ul className="dropdown-menu">
                <li><NavLink className="dropdown-item" to="/customers/list/">Customer List</NavLink></li>
                <li><NavLink className="dropdown-item" to="/customers/new/">Add Customer</NavLink></li>
              </ul>
            </li>
            <li className="nav-item dropdown">
              <NavLink className="nav-link dropdown-toggle" to="/" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                Sales
              </NavLink>
              <ul className="dropdown-menu">
                <li><NavLink className="dropdown-item" to="/sales/list/">Sales List</NavLink></li>
                <li><NavLink className="dropdown-item" to="/sales/new/">Record a Sale</NavLink></li>
              </ul>
            </li>
            <li className="nav-item dropdown">
              <NavLink className="nav-link dropdown-toggle" to="/" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                Technician
              </NavLink>
              <ul className="dropdown-menu">
                <li><NavLink className="dropdown-item" to="/technicians/list/">Technician List</NavLink></li>
                <li><NavLink className="dropdown-item" to="/technicians/new/">Add Technician</NavLink></li>
              </ul>
            </li>
            <li className="nav-item dropdown">
              <NavLink className="nav-link dropdown-toggle" to="/" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                  Service Appointments
              </NavLink>
              <ul className="dropdown-menu">
                <li><NavLink className="dropdown-item" to="/appointments/list/">Service Appointments</NavLink></li>
                <li><NavLink className="dropdown-item" to="/appointments/new/">Add Appointment</NavLink></li>
                <li><NavLink className="dropdown-item" to="/appointments/history/">Service History</NavLink></li>
              </ul>
            </li>
          </ul>
        </div>
      </div>
    </nav>
  )
}

export default Nav;
