import React, { useState } from 'react'

function AutosForm(props) {
    const [ model, setModel] = useState("");
    const [ color, setColor] = useState("");
    const [ year, setYear] = useState("");
    const [ vin, setVin] = useState("");

    const handleModelChange = (event) => {
        const value = event.target.value;
        setModel(value)
    };

    const handleColorChange = (event) => {
        const value = event.target.value;
        setColor(value)
    };

    const handleYearChange = (event) => {
        const value = event.target.value;
        setYear(value)
    };

    const handleVinChange = (event) => {
        const value = event.target.value;
        setVin(value)
    };

    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {};
            data.model_id = model;
            data.color = color;
            data.year = year;
            data.vin = vin;

        const url = 'http://localhost:8100/api/automobiles/'
        const fetchConfig = {
            method: 'post',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json'
            }
        };

        const response = await fetch(url, fetchConfig);
        if (response.ok) {
            await response.json();

            setColor("");
            setYear("");
            setVin("");
            setModel("");
            props.getAutosList()

        } else {
            console.error("bad", response);
        }
    }


    return (
        <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Add Automobile</h1>
            <form onSubmit={handleSubmit} id="create-auto" className="create-auto">
            <div className="form-floating mb-3">
                    <input
                    onChange={handleColorChange}
                    value ={color}
                    placeholder="Color"
                    required
                    type="text"
                    name="color"
                    id="color"
                    className="form-control"
                    />
                <label htmlFor="color">Color</label>
              </div>
              <div className="form-floating mb-3">
                    <input
                    onChange={handleYearChange}
                    value ={year}
                    placeholder="Year"
                    required
                    type="number"
                    name="year"
                    id="year"
                    className="form-control"
                    />
                <label htmlFor="year">Year</label>
              </div>
              <div className="form-floating mb-3">
                    <input
                    onChange={handleVinChange}
                    value ={vin}
                    placeholder="Vin"
                    required
                    type="text"
                    name="vin"
                    id="vin"
                    className="form-control"
                    />
                <label htmlFor="vin">VIN</label>
              </div>
              <div className="form-floating mb-3">
                <select onChange={handleModelChange} name="model" id="model" className="form-select">
                    <option value="">Choose a model</option>
                    {
                        props.modelsList.map(model=> {
                            return (
                                <option key={model.id} value={model.id}>{model.name}</option>
                            )
                        })
                    }
                </select>
                <label htmlFor="model">Model</label>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>

    )
}

export default AutosForm
