
function CustomersList(props) {

    async function deleteCustomer(id) {
        const url = `http://localhost:8090/api/customers/${id}/`;

        const response = await fetch(url, {
          method: 'DELETE',
        });

        if (response.ok) {
          props.setCustomersList(newList => newList.filter(customer => customer.id !== id));
          props.getCustomersList()
        } else {
          console.error("Cannot delete customer", response);
        }
      }

    return (
        <div className="container m-3">
            <div className="row">
            <div className="col-12 col-md-6">
                <h1>Customers</h1>
            </div>
            <div className="table-responsive mt-3">
            <table className="table table-hover">
                <thead className="bg-success text-white">
                    <tr>
                        <th>First Name</th>
                        <th>Last Name</th>
                        <th>Phone Number</th>
                        <th>Address</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    {props.customersList.map((customer)=> {
                        return (
                            <tr key={customer.id}>
                                <td>{customer.first_name}</td>
                                <td>{customer.last_name}</td>
                                <td>{customer.phone_number}</td>
                                <td>{customer.address}</td>
                                <td>
                                    <button onClick={() => deleteCustomer(customer.id)} type="button" className="btn btn-outline-secondary">Delete</button>
                                </td>
                            </tr>
                        )})}
                </tbody>
            </table>
        </div>
        </div>
        </div>

    )
}
export default CustomersList;
