import React, { useState } from 'react'

function CustomersForm(props) {

    const [ first_name, setFirstName] = useState("")
    const [ last_name, setLastName] = useState("")
    const [ address, setAddress] = useState("")
    const [ phone_number, setPhoneNumber] = useState("")

    const handleFirstNameChange = (event) => {
        const value = event.target.value;
        setFirstName(value)
    };

    const handleLastNameChange = (event) => {
        const value = event.target.value;
        setLastName(value)
    };
    const handleAddressChange = (event) => {
        const value = event.target.value;
        setAddress(value)
    };

    const handlePhoneNumberChange = (event) => {
        const value = event.target.value;
        setPhoneNumber(value)
    };

    async function handleSubmit (event) {
        event.preventDefault();
        const data = {};
            data.first_name = first_name;
            data.last_name = last_name;
            data.address = address;
            data.phone_number = phone_number
        const url = "http://localhost:8090/api/customers/"
        const fetchConfig = {
            method: 'POST',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json'
            }
        };
        const response = await fetch(url, fetchConfig)
        if (response.ok) {
            await response.json();

            setFirstName('');
            setLastName('');
            setAddress('');
            setPhoneNumber('');
            props.getCustomersList()
        }
    }

    return (
        <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Add Customer</h1>
            <form onSubmit={handleSubmit} id="create-customer" className="create-customer">
              <div className="form-floating mb-3">
                <input
                onChange={handleFirstNameChange}
                value ={first_name}
                placeholder="first_name"
                required
                type="text"
                name="first_name"
                id="first_name"
                className="form-control"
                />
                <label htmlFor="first_name">First name</label>
            </div>
            <div className="form-floating mb-3">
                <input
                onChange={handleLastNameChange}
                value ={last_name}
                placeholder="last_name"
                required
                type="text"
                name="last_name"
                id="last_name"
                className="form-control"
                />
                <label htmlFor="last_name">Last name</label>
            </div>
          <div className="form-floating mb-3">
                <input
                onChange={handleAddressChange}
                value ={address}
                placeholder="address"
                required
                type="text"
                name="address"
                id="address"
                className="form-control"
                />
            <label htmlFor="address">Address</label>
          </div>
          <div className="form-floating mb-3">
                <input
                onChange={handlePhoneNumberChange}
                value ={phone_number}
                placeholder="phone"
                required
                type="text"
                name="phone"
                id="phone"
                className="form-control"
                />
            <label htmlFor="phone">Phone number: ###-###-####</label>
          </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    )
}
export default CustomersForm;
