import { useEffect, useState } from "react";

function ListAppointments(props) {


      async function finishAppointment(id) {
          const url = `http://localhost:8080/api/appointments/${id}/finish/`;
          const response = await fetch(url, {
            method: "PUT"
          });
          if (response.ok){
            statusAppointments();
          }
        }

      async function cancelAppointment(id) {
        const url = `http://localhost:8080/api/appointments/${id}/cancel/`;
        const response = await fetch(url, {
          method: "PUT"
        });
        if (response.ok) {
          statusAppointments();
        }
      };


      const autos = props.autosList;

      function isVIP(appointment) {
          for (let auto of autos) {
              if (auto.vin === appointment.vin) {
                  return "VIP";
              };
          };
          return "Is Not";
        };

      const [pendingAppointments, setPendingAppointments] = useState([]);
      async function statusAppointments() {
        const url = "http://localhost:8080/api/appointments/";
        const response = await fetch(url);
        if (response.ok) {
          const data = await response.json();
          const filteredAppointments = data.appointments.filter(appointment => appointment.status !== "canceled" && appointment.status !== 'finished');
          setPendingAppointments(filteredAppointments);
        } else {console.error(response)}
      }
      useEffect(() => {
        statusAppointments();
        props.getAppointmentList();
      }, []);

      return (
        <div className="container m-3">
          <div className="row">
          <div className="col-12 col-md-10">
            <h1>Service Appointments</h1>
            <div className="table-responsive mt-3">
            <table className="table table-hover">
                <thead className="bg-success text-white">
                    <tr>
                        <th>Customer</th>
                        <th>VIP</th>
                        <th>VIN</th>
                        <th>Date and time</th>
                        <th>Technician</th>
                        <th>Reason</th>
                        <th>Status</th>
                    </tr>
                </thead>
                <tbody>
                {
                    pendingAppointments.map(appointment => {
                        return(
                            <tr key={appointment.id}>
                                <td>{appointment.customer}</td>
                                <td>{isVIP(appointment)}</td>
                                <td>{appointment.vin}</td>
                                <td>{new Date(appointment.date_time).toLocaleDateString('en-US', {
                                    year: 'numeric',
                                    month: 'short',
                                    day: 'numeric',
                                    hour: 'numeric',
                                    minute: 'numeric',
                                    hour12: true,
                                    })}
                                </td>
                                <td>{appointment.technician.first_name} {appointment.technician.last_name}</td>
                                <td>{appointment.reason}</td>
                                <td>
                                  <button onClick={() => finishAppointment(appointment.id)} type="button" className="btn btn-outline-success" style={{display: 'inline-block'}}>Finish</button>
                                </td>
                                <td>
                                  <button onClick={() => cancelAppointment(appointment.id)} type="button" className="btn btn-outline-secondary" style={{display: 'inline-block'}}>Cancel</button>
                                </td>
                            </tr>
                        )})}
                </tbody>
            </table>
            </div>
          </div>
          </div>
        </div>
    )
}
export default ListAppointments;
