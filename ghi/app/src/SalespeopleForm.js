import React, { useState } from 'react'

function SalespeopleForm(props) {

    const [ first_name, setFirstName] = useState("")
    const [ last_name, setLastName] = useState("")
    const [ employee_id, setEmployeeID] = useState("")

    const handleFirstNameChange = (event) => {
        const value = event.target.value;
        setFirstName(value)
    };

    const handleLastNameChange = (event) => {
        const value = event.target.value;
        setLastName(value)
    };
    const handleEmployeeIDChange = (event) => {
        const value = event.target.value;
        setEmployeeID(value)
    };


    async function handleSubmit (event) {
        event.preventDefault();
        const data = {};
            data.first_name = first_name;
            data.last_name = last_name;
            data.employee_id = employee_id;

        const url = "http://localhost:8090/api/salespeople/"
        const fetchConfig = {
            method: 'POST',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json'
            }
        };
        const response = await fetch(url, fetchConfig)
        if (response.ok) {
            await response.json();

            setFirstName('');
            setLastName('');
            setEmployeeID('');
            props.getSalespeopleList()
        }
    }

    return (
        <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Add Salesperson </h1>
            <form onSubmit={handleSubmit} id="create-salesperson" className="create-salesperson">
              <div className="form-floating mb-3">
                <input
                onChange={handleFirstNameChange}
                value ={first_name}
                placeholder="first_name"
                required
                type="text"
                name="first_name"
                id="first_name"
                className="form-control"
                />
                <label htmlFor="first_name">First name</label>
            </div>
            <div className="form-floating mb-3">
                <input
                onChange={handleLastNameChange}
                value ={last_name}
                placeholder="last_name"
                required
                type="text"
                name="last_name"
                id="last_name"
                className="form-control"
                />
                <label htmlFor="last_name">Last name</label>
            </div>
          <div className="form-floating mb-3">
                <input
                onChange={handleEmployeeIDChange}
                value ={employee_id}
                placeholder="employee_ID"
                required
                type="text"
                name="employee_id"
                id="employee_id"
                className="form-control"
                />
            <label htmlFor="employee_id">EmployeeID</label>
          </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    )
}
export default SalespeopleForm;
