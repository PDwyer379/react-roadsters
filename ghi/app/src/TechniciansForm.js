import { useState } from "react";

function TechniciansForm(props) {

    const [ firstName, setFirstName ] = useState("");
    const [ lastName, setLastName ] = useState("");
    const [ employeeID, setEmployeeID ] = useState("");

    const handleFirstNameChange = (event) => {
        const value = event.target.value;
        setFirstName(value);
    };

    const handleLastNameChange = (event) => {
        const value = event.target.value;
        setLastName(value);
    };

    const handleEmployeeIDChange = (event) => {
        const value = event.target.value;
        setEmployeeID(value);
    };

    async function handleSubmit (event) {
        event.preventDefault();
        const  data = {};
            data.first_name = firstName;
            data.last_name = lastName;
            data.employee_id = employeeID;

        const url = "http://localhost:8080/api/technicians/";
        const fetchConfig = {
            method: "POST",
            body: JSON.stringify(data),
            headers: {
                "Content-Type": "application/json"
            },
        };

        const response = await fetch(url, fetchConfig);
        if (response.ok) {
            await response.json();

            setFirstName("");
            setLastName("");
            setEmployeeID("");
            props.getTechniciansList();
        } else {
            console.error("Error:", response)
        }
    }

    return (
        <div>
            <div className="row">
                <div className="offset-3 col-6">
                    <h1>Add a Technician</h1>
                    <div className="shadow p-4 mt-4">
                        <form onSubmit={handleSubmit} id="create-auto" className="create-auto">
                            <div className="form-floating mb-3">
                                <input onChange={handleFirstNameChange}
                                value={firstName}
                                placeholder="First Name"
                                required type="text"
                                name="firstName"
                                id="firstName"
                                className="form-control"/>
                                <label htmlFor="firstName">First Name</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={handleLastNameChange}
                                value={lastName}
                                placeholder="Last Name"
                                required type="text"
                                name="lastName"
                                id="lastName"
                                className="form-control"/>
                                <label htmlFor="lastName">Last Name</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={handleEmployeeIDChange}
                                value={employeeID}
                                placeholder="Employee ID"
                                required type="text"
                                name="employeeID"
                                id="employeeID"
                                className="form-control"/>
                                <label htmlFor="employeeID">Employee ID</label>
                            </div>
                            <button className="btn btn-primary">Create</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    )
}
export default TechniciansForm;
